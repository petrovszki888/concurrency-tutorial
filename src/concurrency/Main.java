package concurrency;

public class Main {

    public static void main(String[] args) {
        Thread bobThread = new GreetingThread("Bob", 7);
        Thread frankThread = new GreetingThread("Frank", 9);
        Thread anonymusThread = new GreetingThread();

        bobThread.start();
        frankThread.start();
        anonymusThread.start();

        System.out.println("I'm not waiting");
    }

}
